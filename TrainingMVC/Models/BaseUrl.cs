﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models
{
    public class BaseUrl
    {
        static public IConfigurationRoot Configuration { get; set; }
        public BaseUrl()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        public string getMsSQLConnectionString()
        {
            return Configuration["MsSQLConnection"];
        }
    }
}
