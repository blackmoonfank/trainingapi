﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models
{
    public class Employee
    {
        public string nrp { get; set; }
        public string name { get; set; }
        public string address { get; set; }

        public Employee getEmployee(string id)
        {
            Employee emp = new Employee();
            emp.nrp = "1234";
            emp.name = "Anton";
            emp.address = "Pulogadung";
            return emp;
        }
    }
}
