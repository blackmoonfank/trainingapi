﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models.Training
{
    public class TrainingContext : DbContext
    {
        public DbSet<tbl_employee> tbl_employee { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(new BaseUrl().getMsSQLConnectionString());
        }
    }
}
