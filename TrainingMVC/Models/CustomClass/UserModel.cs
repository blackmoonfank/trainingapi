﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models.CustomClass
{
    public class UserModel
    {
        public string Nrp { get; set; }
        public string Name { get; set; }
    }
}
