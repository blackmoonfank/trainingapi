﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingMVC.Models.CustomClass
{
    public class ClaimToken
    {
        public static UserModel claim(HttpRequest sParam)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                string authHeader = sParam.Headers["Authorization"];
                authHeader = authHeader.Replace("Bearer ", "");
                var jsonToken = handler.ReadToken(authHeader);
                var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;

                var _nameid = tokenS.Claims.First(claim => claim.Type == "nameid").Value;
                var _Name = tokenS.Claims.First(claim => claim.Type == "given_name").Value;
                var _Nrp = tokenS.Claims.First(claim => claim.Type == "unique_name").Value;
                var _Distrik = tokenS.Claims.First(claim => claim.Type == "email").Value.Split("|")[0];
                var _Pnrp = tokenS.Claims.First(claim => claim.Type == "email").Value.Split("|")[1];

                return new UserModel() { Name = _Name, Nrp = _Nrp }; 

            }catch(Exception)
            {
                return null;
            } 
        }
    }
}
