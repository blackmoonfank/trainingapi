﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrainingMVC.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrainingMVC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingController : Controller
    {
        [Authorize]
        [HttpGet]
        [Route("getListEmployee")]
        public JsonResult getListEmployee()
        {
            try
            {
                return Json(new tbl_employee().getListEmployee());
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    remarks = "Terjadi kesalahan dalam pengambilan data",
                    err = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("insertEmployee")]
        public JsonResult insertEmployee([FromBody] tbl_employee sParam)
        {
            try
            {
                return Json(new tbl_employee().insertEmployee(sParam));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    remarks = "Terjadi kesalahan dalam penambahan data",
                    err = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("updateEmployee")]
        public JsonResult updateEmployee([FromBody] tbl_employee sParam)
        {
            try
            {
                return Json(new tbl_employee().updateEmployee(sParam));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    remarks = "Terjadi kesalahan dalam penambahan data",
                    err = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("deleteEmployee")]
        public JsonResult deleteEmployee([FromQuery] string nrp)
        {
            try
            {
                return Json(new tbl_employee().deleteEmployee(nrp));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    remarks = "Terjadi kesalahan dalam penambahan data",
                    err = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("insertListEmployee")]
        public JsonResult insertListEmployee([FromBody] List<tbl_employee> sParam)
        {
            try
            {
                return Json(new tbl_employee().insertListEmployee(sParam));
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    remarks = "Terjadi kesalahan dalam penambahan data",
                    err = ex.ToString()
                });
            }
        }

        public class paramList
        {
            public param sparam { get; set; }
            public List<param> list_param { get; set; }
        }

        public class param
        {
            public tbl_employee tbl_emp { get; set; }
            public List<tbl_employee> list_tbl_emp { get; set; }
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
